/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bethalevi.sidur1;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

import org.bethalevi.sidur1.analytics.AnalyticsApplication;
import org.bethalevi.sidur1.database.DatabaseFunctions;
import org.bethalevi.sidur1.objects.Globle;
import org.bethalevi.sidur1.objects.SidurDate;
import org.bethalevi.sidur1.objects.SidurField;
import org.bethalevi.sidur1.view.ObservableWebView;

import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ToolbarControlWebViewActivity extends ToolbarControlBaseActivity<ObservableWebView> {

    private ProgressBar mProgressBar;
    ObservableWebView mWebViewHTML;
    private String mTitle="",url="",mMenuItemMarker="",mScreenName="",mHTMLFileName="";
    public static RelativeLayout mLayoutPregress;
    public static TextView mTextViewPercentage,mTextViewPercentageValue;
    public static float viewWidth;
    private SidurDate mSidurDate;
    private boolean isMarkerLoad = false,isScroll = false,isAllowPercentage=false,isZoom=false;
    public static int mPercentage = 0, mTotalHieght = 0;
    private Tracker mTracker;
    int zoomValue = 90;
    private ActionBar mActionBar;
    @Override
    protected int getLayoutResId() {

        return R.layout.activity_toolbarcontrolwebview;
    }

    public void setActionBarValue(){
        try {
            mActionBar = getSupportActionBar();
            mActionBar.setDisplayUseLogoEnabled(true);
            mActionBar.setDisplayShowHomeEnabled(true);
            mActionBar.setShowHideAnimationEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            //  mActionBar.setLogo(getResources().getDrawable(R.drawable.ic_sidur));
            mActionBar.setTitle(" " + mTitle);
        }catch (Exception e){
            Log.e("ActionBar", "Exception : " + e.toString());
        }
    }

    @Override
    protected ObservableWebView createScrollable() {

        getBundleValues();
        setActionBarValue();
        try {
            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            mTracker = application.getDefaultTracker();

        }catch (Exception e){
            Log.e("AnalyticsApplication", "Exception : " + e.toString());
        }
        try {
            mProgressBar = (ProgressBar)findViewById(R.id.progressbar);
            mLayoutPregress = (RelativeLayout)findViewById(R.id.layout_progressbar);
            mTextViewPercentage = (TextView)findViewById(R.id.textview_percentage);
            mTextViewPercentageValue = (TextView)findViewById(R.id.textview_percentage_value);
        }catch (Exception e){
            Log.e("initGloble", "Exception : " + e.toString());
        }

        try {
            viewWidth = mLayoutPregress.getLayoutParams().width;
            ViewTreeObserver vto = mLayoutPregress.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    viewWidth = mLayoutPregress.getMeasuredWidth();

                }
            });
            new ReadHTMLFile().execute();
        }catch (Exception e){
            Log.e("ViewTreeObserver", "Exception : " + e.toString());
        }

        mWebViewHTML = (ObservableWebView) findViewById(R.id.scrollable);
       // mWebViewHTML.loadUrl("file:///android_asset/02_Einleitung.html");
        return mWebViewHTML;
    }
    public void getBundleValues() {
        try {
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                mTitle = bundle.getString("MenuItemTitle");
                mHTMLFileName = bundle.getString("MenuItemHTMLFileName");
                mMenuItemMarker = bundle.getString("MenuItemMarker");
                mScreenName = bundle.getString("MenuItemScreenName");
                isAllowPercentage = bundle.getBoolean("MenuItemPercentage");
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("mMenuItem", "Exception: " + e.toString());

        }
    }

    class ReadHTMLFile extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            try {
                mProgressBar.setVisibility(View.VISIBLE);

            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            // TODO Auto-generated method stub
            String msg = "Error";
            try {
                readHTMLFileTag();
            } catch (Exception e) {
                Log.e("ReadHTMLFile", "Exception: " + e.toString());
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            try {
                loadHtml();
                mProgressBar.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    public void readHTMLFileTag(){
        String s ="",newURL="";
        String hideList = "";
        String sArray[] ;
        try{
            InputStream htmlFile =getAssets().open(mHTMLFileName);
            int size = htmlFile.available();
            byte[] buffer = new byte[size];
            htmlFile.read(buffer);
            htmlFile.close();
            s = new String(buffer);
          //  s= s.replace("<body>", "<body><br><br><br>");

        }catch (Exception e){
            Log.e("readHTMLFileTag","InputStream Exception: "+e.toString());
        }

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(s);
            hideList = sb.substring(sb.indexOf("<!--(")+5,sb.indexOf(")-->"));

        }catch (Exception e){
            Log.e("readHTMLFileTag","hideList Exception: "+e.toString());
        }
        try{
            newURL = s;
            mSidurDate = DatabaseFunctions.getSidurDate("" + Globle.getCurrentDate());
            int arraysize = mSidurDate.getArrayListSidurField().size();
            SidurField sidur;
            for (int i = 0 ; i < arraysize; i ++){
                sidur = mSidurDate.getArrayListSidurField().get(i);
                if (sidur.getValue() == 0){
                    Pattern p = Pattern.compile(sidur.getPattern(), Pattern.DOTALL);
                    Matcher m = p.matcher(newURL.toString());
                    while (m.find()) {
                        newURL=newURL.replace(m.group(),"");
                    }
                }
            }
        }catch (Exception e){
            Log.e("readHTMLFileTag","Pattern Exception: "+e.toString());
        }
        url = newURL;

    }

    public void loadHtml() {

        mWebViewHTML.getSettings().setJavaScriptEnabled(true);
        mWebViewHTML.setHorizontalScrollBarEnabled(false);
        mWebViewHTML.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        mWebViewHTML.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        mWebViewHTML.setLongClickable(false);
        mWebViewHTML.setHapticFeedbackEnabled(false);
        ToolbarControlWebViewActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mWebViewHTML.loadDataWithBaseURL("file:///android_asset/", url, "text/html", "utf-8", null);
                return;
            }
        });
        mWebViewHTML.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(final WebView view, final String url) {

                super.onPageFinished(view, url);
                Handler lHandler = new Handler();
                lHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!mHTMLFileName.equals("")) {
                            if (mHTMLFileName.equals("03_SHACHRIT.html")) {
                                if (!isMarkerLoad) {
                                    String id = "#" + mMenuItemMarker;
                                    view.loadUrl(url + id);
                                    isMarkerLoad = true;
                                    isScroll = true;
                                }

                            } else {
                                view.scrollTo(0, 1);
                                mWebViewHTML.scrollTo(0, 1);
                            }
                        }
                    }
                }, 600);
            }


        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.zoom, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_zoom_in:
                zoomValue = zoomValue + 20;
                if (zoomValue > 250)
                    zoomValue = 250;
                try {
                    isZoom = true;
                    mWebViewHTML.getSettings().setTextZoom(zoomValue);

                } catch (Exception e) {
                    mWebViewHTML.getSettings().setTextZoom(zoomValue);
                }
                return true;
            case R.id.action_zoom_out:
                zoomValue = zoomValue - 20;
                if (zoomValue < 80)
                    zoomValue = 70;
                try {
                    isZoom = true;
                    // mWebViewHTML.zoomOut();
                    mWebViewHTML.getSettings().setTextZoom(zoomValue);
                }catch (Exception e){
                    mWebViewHTML.getSettings().setTextZoom(zoomValue);
                }
                return true;
            case 16908332:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
