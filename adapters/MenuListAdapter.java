package org.bethalevi.sidur1.adapters;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.bethalevi.sidur1.R;
import org.bethalevi.sidur1.objects.CircularImageView;
import org.bethalevi.sidur1.objects.MenuItem;

import java.util.ArrayList;

public class MenuListAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<MenuItem> mMenuItemList;
	private LayoutInflater mInflater;

	public MenuListAdapter(Context context, ArrayList<MenuItem> entries) {
		// TODO Auto-generated constructor stub
		this.context = context;
		this.mMenuItemList = entries;
		this.mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		try {
			return mMenuItemList.size();
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mMenuItemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@TargetApi(Build.VERSION_CODES.M)
	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;

		try {
			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.list_item_main, null);
				holder = new ViewHolder();
				holder.textTrans = (TextView) convertView
						.findViewById(R.id.textview_trans);
				holder.textTitle = (TextView) convertView
						.findViewById(R.id.textview_title);
				holder.imageViewDP = (CircularImageView) convertView
						.findViewById(R.id.imageview_dp);

				holder.mLayoutBorder = (RelativeLayout)convertView.findViewById(R.id.layout_image);
				convertView.setTag(holder);
			} else
				holder = (ViewHolder) convertView.getTag();

			try {
				if (mMenuItemList.get(position).getTitle().equals("Im Gedenken") || mMenuItemList.get(position).getTitle().equals("Einleitung") || mMenuItemList.get(position).getTitle().equals("weitere Gebete")){
					holder.textTrans.setText(""+mMenuItemList.get(position).getTitle());
					holder.textTitle.setText("" + mMenuItemList.get(position).getTitleTrans());
				}else{
					holder.textTrans.setText(""+mMenuItemList.get(position).getTitleTrans());
					holder.textTitle.setText("" + mMenuItemList.get(position).getTitle());
				}
				holder.imageViewDP.setImageDrawable(mMenuItemList.get(position).getImageUrl());

				try {

					/*int colorCode = mMenuItemList.get(position).getColorCode();
					holder.textTrans.setTextColor(colorCode);
					holder.textTitle.setTextColor(colorCode);
					GradientDrawable bgShape = (GradientDrawable)holder.mLayoutBorder.getBackground();
					bgShape.setStroke(3, colorCode);*/

					int colorCode = context.getResources().getColor(R.color.text_circle_color);
					holder.textTrans.setTextColor(colorCode);
					holder.textTitle.setTextColor(colorCode);
					GradientDrawable bgShape = (GradientDrawable)holder.mLayoutBorder.getBackground();
					bgShape.setStroke(3, colorCode);

				}catch (Exception e) {
					// TODO: handle exception
					Log.e("GradientDrawable","Exception: "+e.toString());
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			final Activity activity = (Activity) context;
		} catch (Exception e) {
			// TODO: handle exception
		}

		return convertView;
	}

	private class ViewHolder {
		TextView textTitle, textTrans;
		CircularImageView imageViewDP;
		RelativeLayout mLayoutBorder;


	}


}
