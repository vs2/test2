package org.bethalevi.sidur1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.bethalevi.sidur1.adapters.MenuListAdapter;
import org.bethalevi.sidur1.analytics.AnalyticsApplication;
import org.bethalevi.sidur1.database.DatabaseFunctions;
import org.bethalevi.sidur1.objects.MenuItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView mListViewMenuItem;
    private ArrayList<MenuItem> mMenuItemList;
    private MenuListAdapter mAdapter;
    int back = 0;
    private Tracker mTracker;
    private TextView mTextViewHeading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            getSupportActionBar().hide();
            DatabaseFunctions.openDB(MainActivity.this);
            //DatabaseFunctions.getSidurDate(Globle.getCurrentDate());

        }catch (Exception e){
            Log.e("ActionBar", "Exception : " + e.toString());
        }
        try {
            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            mTracker = application.getDefaultTracker();

        }catch (Exception e){
            Log.e("AnalyticsApplication", "Exception : " + e.toString());
        }


       initGloble();

    }

    public void initGloble(){

        mTextViewHeading = (TextView)findViewById(R.id.textview_heading);
        mListViewMenuItem = (ListView)findViewById(R.id.lisview_menu);
        mMenuItemList = MenuItem.getMenuItemsArrayList(MainActivity.this);
        mAdapter = new MenuListAdapter(MainActivity.this,mMenuItemList);
        mListViewMenuItem.setAdapter(mAdapter);

        mListViewMenuItem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                try {
                    mTracker.setScreenName("" + mMenuItemList.get(position).getAnalyticScreenName());
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Action")
                            .setAction("Click")
                            .build());

                }catch (Exception e){
                    Log.e("mTracker setScreenName", "Exception : " + e.toString());
                }


               if (mMenuItemList.get(position).getTitle().equals("SHACHRIT")){
                   mTextViewHeading.setText("SHACHRIT");
                   back= 1;
                   mMenuItemList.clear();
                   mMenuItemList = MenuItem.getShachritSubMenuList(MainActivity.this);
                   mAdapter = new MenuListAdapter(MainActivity.this,mMenuItemList);
                   mListViewMenuItem.setAdapter(mAdapter);
                   mAdapter.notifyDataSetChanged();

               }else if(mMenuItemList.get(position).getTitle().equals("weitere Gebete")){
                   mTextViewHeading.setText("weitere Gebete");
                   back = 2;
                   mMenuItemList.clear();
                   mMenuItemList = MenuItem.getWeitereGebeteSubMenuList(MainActivity.this);
                   mAdapter = new MenuListAdapter(MainActivity.this,mMenuItemList);
                   mListViewMenuItem.setAdapter(mAdapter);
                   mAdapter.notifyDataSetChanged();
               }else{
                  // Intent i =new Intent(MainActivity.this,WebViewActivity.class);
                   Intent i =new Intent(MainActivity.this,ToolbarControlWebViewActivity.class);

                   if (mTextViewHeading.getText().toString().equals("SHACHRIT")){
                       i.putExtra("MenuItemTitle", "SHACHRIT");
                   }else if (mTextViewHeading.getText().toString().equals("weitere Gebete")){
                       i.putExtra("MenuItemTitle", "weitere Gebete");
                   }else{
                       i.putExtra("MenuItemTitle", mMenuItemList.get(position).getTitle());
                   }
                   i.putExtra("MenuItemHTMLFileName", mMenuItemList.get(position).getHtmlFileName());
                   i.putExtra("MenuItemScreenName", mMenuItemList.get(position).getAnalyticScreenName());
                   i.putExtra("MenuItemMarker", mMenuItemList.get(position).getMarker());
                   i.putExtra("MenuItemPercentage", mMenuItemList.get(position).isAllowPercentage());
                   startActivity(i);
               }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (back == 0) {
            super.onBackPressed();
        }else{
            mTextViewHeading.setText("SIDDUR TEHILAT AVNER");
            back = 0;
            mMenuItemList.clear();
            mMenuItemList = MenuItem.getMenuItemsArrayList(MainActivity.this);
            mAdapter = new MenuListAdapter(MainActivity.this,mMenuItemList);
            mListViewMenuItem.setAdapter(mAdapter);
        }
    }
}
