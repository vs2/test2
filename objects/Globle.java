package org.bethalevi.sidur1.objects;

import android.util.Log;

import java.util.Calendar;

/**
 * Created by Mitesh Patel on 10/29/2015.
 */
public class Globle {

    public static final String AssetPath = "file:///android_asset/";

    public static String getCurrentDate(){
        try {
            Calendar c = Calendar.getInstance();
            String day="",month="";
            day = c.get(Calendar.DAY_OF_MONTH)+"";

            if (c.get(Calendar.DAY_OF_MONTH) < 10){
               if (day.contains("0")){


               }else{
                   day = ""+c.get(Calendar.DAY_OF_MONTH);
                   // day = day.replace("0","");
                   day = "0"+day;
               }
            }

            int m = c.get(Calendar.MONTH) + 1;
            month = ""+m;
           if (m < 10){
              if (month.contains("0")){

              }else{
                  month = ""+m;
                  // month = month.replace("0","");
                  month = "0"+month;
              }
            }
            //String date  = day+"/"+month+"/"+c.get(Calendar.YEAR);
            String date  = month+"/"+day+"/"+c.get(Calendar.YEAR);
            Log.e("getCurrentDate", "Current day => " + day);
            Log.e("getCurrentDate", "Current month => " + month);
            //SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            //String formattedDate = df.format(c.getTime());
            Log.e("getCurrentDate", "Current formattedDate => " + date);
            return  date;
        }catch (Exception e){
            Log.e("getCurrentDate", "Exception: " + e.toString());
            return null;
        }
    }

}
