package org.bethalevi.sidur1.objects;

import java.util.ArrayList;

/**
 * Created by Mitesh Patel on 11/4/2015.
 */
public class SidurDate {

    String date,Desc;
    ArrayList<SidurField> arrayListSidurField;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public ArrayList<SidurField> getArrayListSidurField() {
        return arrayListSidurField;
    }

    public void setArrayListSidurField(ArrayList<SidurField> arrayListSidurField) {
        this.arrayListSidurField = arrayListSidurField;
    }

    public SidurDate() {
    }

}
