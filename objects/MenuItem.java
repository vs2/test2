package org.bethalevi.sidur1.objects;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import org.bethalevi.sidur1.R;

import java.util.ArrayList;

/**
 * Created by Mitesh Patel on 10/27/2015.
 */
public class MenuItem implements Parcelable {

    String title, titleTrans, htmlFileName, marker, analyticScreenName;
    Drawable imageUrl;
    int colorCode;
    boolean allowPercentage;


    public String getHtmlFileName() {
        return htmlFileName;
    }

    public void setHtmlFileName(String htmlFileName) {
        this.htmlFileName = htmlFileName;
    }

    public String getTitle() {
        return title;
    }

    public int getColorCode() {
        return colorCode;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleTrans() {
        return titleTrans;
    }

    public void setTitleTrans(String titleTrans) {
        this.titleTrans = titleTrans;
    }

    public Drawable getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Drawable imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public String getAnalyticScreenName() {
        return analyticScreenName;
    }

    public void setAnalyticScreenName(String analyticScreenName) {
        this.analyticScreenName = analyticScreenName;
    }

    public boolean isAllowPercentage() {
        return allowPercentage;
    }

    public void setAllowPercentage(boolean allowPercentage) {
        this.allowPercentage = allowPercentage;
    }

    public MenuItem() {
    }

    public static ArrayList<MenuItem> getMenuItemsArrayList(Context context) {
        ArrayList<MenuItem> menuItemList = new ArrayList<MenuItem>();
        try {


            MenuItem menuItem = new MenuItem();
            menuItem.setTitle("Im Gedenken");
            menuItem.setTitleTrans("");
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("01_Im Gedenken");
            menuItem.setAllowPercentage(false);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("Einleitung");
            menuItem.setTitleTrans("");
            menuItem.setColorCode(context.getResources().getColor(R.color.green));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("02_Einleitung.html");
            menuItem.setAnalyticScreenName("02_Einleitung");
            menuItem.setAllowPercentage(false);
            menuItem.setMarker("");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SHACHRIT");
            menuItem.setTitleTrans("Morgengebet für Wochentage");
            menuItem.setColorCode(context.getResources().getColor(R.color.orange));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.shacharit));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setMarker("");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("MINĊHA");
            menuItem.setTitleTrans("Nachmittagsgebet für Wochentage");
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("04_MINCHA.html");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("04_MINCHA");
            menuItem.setAllowPercentage(true);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("ĀRWIT");
            menuItem.setTitleTrans("Abendgebet für Wochentage");
            menuItem.setColorCode(context.getResources().getColor(R.color.black));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.arwit));
            menuItem.setHtmlFileName("05_ARWIT.html");
            menuItem.setAnalyticScreenName("05_ARWIT");
            menuItem.setAllowPercentage(true);
            menuItem.setMarker("");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("KRIAT SCHeMĀ ĀL HAMITA");
            menuItem.setTitleTrans("Gebet vor dem Schlafengehen");
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("06_KRIAT SCHeMA AL HAMITA.html");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("06_KRIAT SCHeMA AL HAMITA");
            menuItem.setAllowPercentage(false);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SCHIR HASCHIRIM");
            menuItem.setTitleTrans("Das Hohelied der Liebe");
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.shir_hashirim));
            menuItem.setHtmlFileName("07_SCHIR HASCHIRIM.html");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("07_SCHIR HASCHIRIM");
            menuItem.setAllowPercentage(false);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("BIRKAT HAMAŚON");
            menuItem.setTitleTrans("Das Tischgebet");
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("08_BIRKAT HAMASON.html");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("08_BIRKAT HAMAŚON");
            menuItem.setAllowPercentage(true);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("BIRKOT HANEHENIN");
            menuItem.setTitleTrans("Danksagung für den Genuss");
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.birkat_hanehenin));
            menuItem.setHtmlFileName("09_BIRKOT HANEHENIN.html");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("09_BIRKOT HANEHENIN");
            menuItem.setAllowPercentage(false);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("BIRKAT MEĒN SCHALOSCH");
            menuItem.setTitleTrans("Drei Danksagungen");
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.meen_schalosch));
            menuItem.setHtmlFileName("10_BIRKAT MEEN SCHALOSCH.html");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("10_BIRKAT MEĒN SCHALOSCH");
            menuItem.setAllowPercentage(false);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SEDER HAWDALA");
            menuItem.setTitleTrans("Unterscheidungssegen");
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("11_SEDER HAWDALA.html");
            menuItem.setMarker("");
            menuItem.setAnalyticScreenName("11_SEDER HAWDALA");
            menuItem.setAllowPercentage(false);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("weitere Gebete");
            menuItem.setTitleTrans("");
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("");
            menuItem.setAnalyticScreenName("");
            menuItem.setAllowPercentage(false);
            menuItem.setMarker("");
            menuItemList.add(menuItem);


        } catch (Exception e) {

        }
        return menuItemList;
    }

    public static ArrayList<MenuItem> getShachritSubMenuList(Context context) {
        ArrayList<MenuItem> menuItemList = new ArrayList<MenuItem>();
        try {

            MenuItem menuItem = new MenuItem();
            menuItem.setTitle("BIRKOT HASCHAĊHAR");
            menuItem.setTitleTrans("Danksagungen am Morgen");
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS01YYY");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.birkot_hashachar));
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("ĀTIFAT ZIZIT");
            menuItem.setTitleTrans("Anziehen des Gebetsmantels");
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS02YYY");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.green));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.atifat_zizit));

            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("HANAĊHAT TeFILIN");
            menuItem.setTitleTrans("Anlegen der Tefilin");
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS03YYY");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.orange));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.tefillin));

            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("KORBANOT");
            menuItem.setTitleTrans("Opfer");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS04YYY");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("PSUKI DESMIRA");
            menuItem.setTitleTrans("Lobsagungen");
            menuItem.setColorCode(context.getResources().getColor(R.color.black));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setMarker("YYYPOS05YYY");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SHMA ISRAEL");
            menuItem.setTitleTrans("Schma Israel Gebet");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.shma_israel));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS06YYY");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TFILAT AMIDA");
            menuItem.setTitleTrans("Amida Gebet");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.tfilat_amida));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS07YYY");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TACHNUN");
            menuItem.setTitleTrans("Bußgebet");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.tachanun));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS08YYY");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TORAH");
            menuItem.setTitleTrans("Thoraaushebung");
            menuItem.setMarker("YYYPOS09YYY");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.torah));
            menuItem.setHtmlFileName("03_SHACHRIT.html");

            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SHIR SHEL YOM");
            menuItem.setTitleTrans("Lied des Tages");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS10YYY");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SOF TEFILA");
            menuItem.setTitleTrans("Gebetsende");
            menuItem.setAnalyticScreenName("03_SHACHRIT");
            menuItem.setAllowPercentage(true);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("03_SHACHRIT.html");
            menuItem.setMarker("YYYPOS11YYY");
            menuItemList.add(menuItem);

        } catch (Exception e) {

        }
        return menuItemList;
    }

    public static ArrayList<MenuItem> getWeitereGebeteSubMenuList(Context context) {
        ArrayList<MenuItem> menuItemList = new ArrayList<MenuItem>();
        try {

            MenuItem menuItem = new MenuItem();
            menuItem.setTitle("SCHeLOSCHA ĀSAR ĪKARIM");
            menuItem.setTitleTrans("Die dreizehn Grundsätze");
            menuItem.setAnalyticScreenName("12_SCHeLOSCHA ĀSAR ĪKARIM");menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("12_SCHeLOSCHA ASAR IKARIM.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("ĒSER ŚeCHIROT");
            menuItem.setTitleTrans("Die zehn Erinnerungen");
            menuItem.setAnalyticScreenName("13_ĒSER ŚeCHIROT");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.green));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("13_ESER SeCHIROT.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("HATAWAT ĊHALOM");
            menuItem.setTitleTrans("Wiedergutmachung des Traumes");
            menuItem.setAnalyticScreenName("14_HATAWAT ĊHALOM");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.orange));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("14_HATAWAT CHALOM.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TeFILA ĀL HAPARNASA");
            menuItem.setTitleTrans("Das Gebet für Geschäftserfolg");
            menuItem.setAnalyticScreenName("15_TeFILA ĀL HAPARNASA");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("15_TeFILA AL HAPARNASA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TeFILA NEGED ĀJIN HARĀ ");
            menuItem.setTitleTrans("Gebet gegen „Böse Augen“");
            menuItem.setAnalyticScreenName("16_TeFILA NEGED ĀJIN HARĀ ");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.black));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("16_TeFILA NEGED AJIN HARA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("PTIĊHAT ELIJAHU ");
            menuItem.setTitleTrans("Elijahus Einleitung");
            menuItem.setAnalyticScreenName("17_PTIĊHAT ELIJAHU ");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("17_PTICHAT ELIJAHU.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SeFIRAT HAŌMER");
            menuItem.setTitleTrans("ŌMER-Zählen ");
            menuItem.setAnalyticScreenName("18_SeFIRAT HAŌMER");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("18_SEFIRAT HAOMER.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("BIRKAT HALeWANA");
            menuItem.setTitleTrans("Segnung des Neumondes ");
            menuItem.setAnalyticScreenName("19_BIRKAT HALeWANA");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("19_BIRKAT HALeWANA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("ĒRUW TAWSCHILIN ");
            menuItem.setTitleTrans("Mischung von Speisen");
            menuItem.setAnalyticScreenName("20_ĒRUW TAWSCHILIN ");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("20_ERUW TAWSCHILIN.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("BIRKOT HA-SCHEWAĊH");
            menuItem.setTitleTrans("Danksagung für das Besondere");
            menuItem.setAnalyticScreenName("21_BIRKOT HA-SCHEWAĊH");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("21_BIRKOT HA-SCHEWACH.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SCHACHARIT LeROSCH ĊHODESCH");
            menuItem.setTitleTrans("Morgengebet für den Neumondtag und das Hallel");
            menuItem.setAnalyticScreenName("22_SCHACHARIT LeROSCH ĊHODESCH");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("22_SCHACHARIT LeROSCH CHODESCH.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("MUSAF LeROSCH ĊHODESCH");
            menuItem.setTitleTrans("Zusatzgebet für den Neumondtag");
            menuItem.setAnalyticScreenName("23_MUSAF LeROSCH ĊHODESCH");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("23_MUSAF LeROSCH CHODESCH.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("CHANUKA");
            menuItem.setTitleTrans("Das Chanukka - Fest");
            menuItem.setAnalyticScreenName("24_CHANUKA");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("24_CHANUKA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("PURIM");
            menuItem.setTitleTrans("Das Purim - Fest");
            menuItem.setAnalyticScreenName("25_PURIM");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.green));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("25_PURIM.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("BIRKAT HA-ILANOT ");
            menuItem.setTitleTrans("Danksagung für blühende Bäume");
            menuItem.setAnalyticScreenName("26_BIRKAT HA-ILANOT ");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.orange));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("26_BIRKAT HA-ILANOT.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SEDER HABRIT");
            menuItem.setTitleTrans("Die Beschneidungszeremonie");
            menuItem.setAnalyticScreenName("27_SEDER HABRIT");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("27_SEDER HABRIT.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SEDER PIDJON HA-BEN");
            menuItem.setTitleTrans("Die Auslösung des Erstgeborenen");
            menuItem.setAnalyticScreenName("28_SEDER PIDJON HA-BEN");menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.black));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("28_SEDER PIDJON HA-BEN.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SEDER KIDUSCHIN");
            menuItem.setTitleTrans("Die Hochzeitszeremonie ");
            menuItem.setAnalyticScreenName("29_SEDER KIDUSCHIN");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("29_SEDER KIDUSCHIN.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SCHEWĀ BeRACHOT ");
            menuItem.setTitleTrans("Die sieben Segensprüche");
            menuItem.setAnalyticScreenName("30_SCHEWĀ BeRACHOT ");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("30_SCHEWA BeRACHOT.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("HADRAT ŚeKENIM");
            menuItem.setTitleTrans("Danksagung für Senioren");
            menuItem.setAnalyticScreenName("31_HADRAT ŚeKENIM");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("31_HADRAT SeKENIM.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("WIDUJ SCHCHIW MERĀ");
            menuItem.setAnalyticScreenName("32_WIDUJ SCHCHIW MERĀ");
            menuItem.setAllowPercentage(false);
            menuItem.setTitleTrans("Glaubensbekenntnis für im Sterben liegende Menschen");
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("32_WIDUJ SCHCHIW MERA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SCHAĀT HAGeSISA");
            menuItem.setTitleTrans("Die Stunde des Dahinscheidens");
            menuItem.setAnalyticScreenName("33_SCHAĀT HAGeSISA");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("33_SCHAAT HAGeSISA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("BIRKAT HAMAŚON BeWET HA-AWEL");
            menuItem.setTitleTrans("Das Tischgebet im Trauerhaus");
            menuItem.setAnalyticScreenName("34_BIRKAT HAMAŚON BeWET HA-AWEL");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("34_BIRKAT HAMASON BeWET HA-AWEL.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("ZIDUK HADIN");
            menuItem.setTitleTrans("G´ttes Urteilsannahme");
            menuItem.setAnalyticScreenName("35_ZIDUK HADIN");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("35_ZIDUK HADIN.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("JOM HASCHWIĪ LeAWELUT");
            menuItem.setTitleTrans("Gedenkgebet für die Toten");
            menuItem.setAnalyticScreenName("36_JOM HASCHWIĪ LeAWELUT");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("36_JOM HASCHWII LeAWELUT.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("HASCHKAWOT Le-ISCH");
            menuItem.setTitleTrans("Das Trauergebet für den Mann");
            menuItem.setAnalyticScreenName("37_HASCHKAWOT Le-ISCH");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.green));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("37_HASCHKAWOT Le-ISCH.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("HASCHKAWOT Le-ISCHA");
            menuItem.setTitleTrans("Das Trauergebet für die Frau");
            menuItem.setAnalyticScreenName("38_HASCHKAWOT Le-ISCHA");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.orange));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("38_HASCHKAWOT Le-ISCHA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("HAFRASCHAT ĊHALA ");
            menuItem.setTitleTrans("Absonderung des Chalateiges");
            menuItem.setAnalyticScreenName("39_HAFRASCHAT ĊHALA ");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("39_HAFRASCHAT CHALA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TeWILAT HANIDA");
            menuItem.setTitleTrans("Das rituelle Bad der Frau");
            menuItem.setAnalyticScreenName("40_TeWILAT HANIDA");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.black));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("40_TeWILAT HANIDA.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TeWILAT KELIM");
            menuItem.setTitleTrans("Die rituelle Reinigung der Gefäße");
            menuItem.setAnalyticScreenName("41_TeWILAT KELIM");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("41_TeWILAT KELIM.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("MEŚUŚA");
            menuItem.setTitleTrans("Vorschriften für die MEŚUŚA");
            menuItem.setColorCode(context.getResources().getColor(R.color.marron));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("42_MESUSA.html");
            menuItem.setAnalyticScreenName("42_MEŚUŚA");menuItem.setAllowPercentage(false);
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("MAĀKE");
            menuItem.setAnalyticScreenName("43_MAĀKE");
            menuItem.setAllowPercentage(false);
            menuItem.setTitleTrans("Danksagung für das Gelände am Dach");
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("43_MAAKE.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("SEDER HAJOZE LADERECH");
            menuItem.setTitleTrans("Vorschriften für den Reisenden");
            menuItem.setAnalyticScreenName("44_SEDER HAJOZE LADERECH");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.yellow));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("44_SEDER HAJOZE LADERECH.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TeFILAT HADERECH");
            menuItem.setTitleTrans("Das Gebet der Reise");
            menuItem.setAnalyticScreenName("45_TeFILAT HADERECH");
            menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.blue));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("45_TeFILAT HADERECH.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("TeFILAT HA-AWIR");
            menuItem.setTitleTrans("Gebet vor dem Flug");
            menuItem.setAnalyticScreenName("46_TeFILAT HA-AWIR");menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("46_TeFILAT HA-AWIR.html");
            menuItemList.add(menuItem);

            menuItem = new MenuItem();
            menuItem.setTitle("KADISCH");
            menuItem.setTitleTrans("Kadisch");
            menuItem.setAnalyticScreenName("47_KADISCH");menuItem.setAllowPercentage(false);
            menuItem.setColorCode(context.getResources().getColor(R.color.red));
            menuItem.setImageUrl(context.getResources().getDrawable(R.drawable.mincha));
            menuItem.setHtmlFileName("47_KADISCH.html");
            menuItemList.add(menuItem);

        } catch (Exception e) {


        }
        return menuItemList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.titleTrans);
        dest.writeString(this.htmlFileName);
        dest.writeInt(this.colorCode);

    }

    public static final Parcelable.Creator<MenuItem> CREATOR = new Parcelable.Creator<MenuItem>() {

        @Override
        public MenuItem createFromParcel(Parcel source) {
            return new MenuItem(source);
        }

        @Override
        public MenuItem[] newArray(int size) {
            return new MenuItem[size];
        }
    };

    public MenuItem(Parcel source) {
        this.title = source.readString();
        this.titleTrans = source.readString();
        this.htmlFileName = source.readString();
    }
}
