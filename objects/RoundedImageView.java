package org.bethalevi.sidur1.objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedImageView extends ImageView {


    public RoundedImageView(Context ctx, AttributeSet attrs) {
        super(ctx, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Drawable drawable = getDrawable();

        if (drawable == null) {
            return;
        }

        if (getWidth() == 0 || getHeight() == 0) {
            return;
        }
        Bitmap b = ((BitmapDrawable) drawable).getBitmap();
        Bitmap bitmap = b.copy(Bitmap.Config.ARGB_8888, true);

        int w = getWidth(), h = getHeight();

        Bitmap roundBitmap = getRoundedCroppedBitmap(bitmap, w,getContext());
        canvas.drawBitmap(roundBitmap, 0, 0, null);

    }

    public static Bitmap getRoundedCroppedBitmap(Bitmap bitmap, int radius,Context c) {
        Bitmap finalBitmap;
        if (bitmap.getWidth() != radius || bitmap.getHeight() != radius)
            finalBitmap = Bitmap.createScaledBitmap(bitmap, radius, radius,
                    false);
        else
            finalBitmap = bitmap;
        Bitmap output = Bitmap.createBitmap(finalBitmap.getWidth(),
                finalBitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
       final Rect rect = new Rect(0, 0, finalBitmap.getWidth(), finalBitmap.getHeight());
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        //paint.setColor(Color.parseColor("#BAB399"));
        paint.setColor(Color.parseColor("#6D6D6D"));

        canvas.drawCircle(finalBitmap.getWidth() / 2 + 0.7f,
                finalBitmap.getHeight() / 2 + 0.7f,
                finalBitmap.getWidth() / 2 + 0.1f, paint);


        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

/*
        //new shadow
        float canvasSize = canvas.getWidth() < canvas.getHeight() ? canvas.getWidth() : canvas.getHeight();
        int outerWidth = (int) (2 * c.getResources().getDisplayMetrics().density + 0.5f);
        int center = (int) (canvasSize / 2);

        Paint backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(0xFF000000);
        canvas.drawCircle(center + outerWidth, center + outerWidth, ((canvasSize - (outerWidth * 2)) / 2), backgroundPaint);
        *//*canvas.drawCircle(roundBitmap.getWidth() / 2 + 0.7f,
                roundBitmap.getHeight() / 2 + 0.7f,
                roundBitmap.getWidth() / 2 + 0.1f, backgroundPaint);*//*



        Shader bitmapShader = new BitmapShader(finalBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        if(canvasSize != finalBitmap.getWidth() || canvasSize != finalBitmap.getHeight()) {
            Matrix matrix = new Matrix();
            float scale = (float) canvasSize / (float) finalBitmap.getHeight();
            //float scale = (float) image.getHeight() / (float) image.getWidth();
            matrix.setScale(scale, scale);
            bitmapShader.setLocalMatrix(matrix);
        }
        Paint paintBorder = new Paint();
        paintBorder.setAntiAlias(true);
        paintBorder.setStyle(Paint.Style.STROKE);
        paintBorder.setShadowLayer(4f, 0f, 2f, Color.BLACK);

        Paint paintSelectorBorder = new Paint();
        paintSelectorBorder.setAntiAlias(true);
        paintSelectorBorder.setShadowLayer(4f, 0f, 2f, Color.BLACK);


        RectF rekt = new RectF(0 + outerWidth / 2, 0 + outerWidth / 2, canvasSize - outerWidth / 2, canvasSize - outerWidth / 2);
        canvas.drawArc(rekt, 360, 360, false, paintBorder);

        canvas.drawCircle(center + outerWidth, center + outerWidth, ((canvasSize - (outerWidth * 2)) / 2) + outerWidth - 4.0f, paintSelectorBorder);


        Paint imagePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        imagePaint.setColor(0xFF000000);
        imagePaint.setMaskFilter(new BlurMaskFilter(12, BlurMaskFilter.Blur.INNER));
        imagePaint.setShader(bitmapShader);

       *//* canvas.drawCircle(roundBitmap.getWidth() / 2 + 0.7f,
                roundBitmap.getHeight() / 2 + 0.7f,
                roundBitmap.getWidth() / 2 + 11.1f, imagePaint);*//*

        canvas.drawCircle(center + outerWidth, center + outerWidth, ((canvasSize - (outerWidth * 2)) / 2), imagePaint);*/

       canvas.drawBitmap(finalBitmap, rect, rect, paint);


        return output;
    }
}
