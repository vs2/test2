package org.bethalevi.sidur1.objects;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.WebView;

import org.bethalevi.sidur1.R;
import org.bethalevi.sidur1.WebViewActivity;

public class ObservableWebView extends WebView
{
    private OnScrollChangedCallback mOnScrollChangedCallback;

    public ObservableWebView(final Context context)
    {
        super(context);
    }

    public ObservableWebView(final Context context, final AttributeSet attrs)
    {
        super(context, attrs);
    }

    public ObservableWebView(final Context context, final AttributeSet attrs, final int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onScrollChanged(final int l, final int t, final int oldl, final int oldt)
    {

        // http://www.gaanza.com/blog/measure-view-android/

        try {
            int heightTotal = (int) Math.floor(this.getContentHeight() * this.getScale());
            int webViewHeight = this.getBottom() - this.getTop();
            int viewHieght = t+webViewHeight;

            int percent=0;
            percent = 100 * t;
            heightTotal = heightTotal - webViewHeight;
            //percent = 100 * totalHieght;
            percent = percent / heightTotal;
            //Log.e("TAG","Percentage: "+percent);

            WebViewActivity.mTotalHieght = heightTotal;
            WebViewActivity.mPercentage = percent;

            Log.e("TAG", "t: " + t);
            Log.e("TAG", "viewHieght: " + viewHieght);
            Log.e("TAG", "heightTotal: " + heightTotal);
            Log.e("TAG", "webViewHeight: " + webViewHeight);


            float width = 0;
            width = (float) ( WebViewActivity.viewWidth / 100 );
            width = width * percent;
            int w = (int) width;
            ViewGroup.LayoutParams lp = WebViewActivity.mTextViewPercentage.getLayoutParams();
            lp.width=w;
           // lp.height=25;
            WebViewActivity.mTextViewPercentage.setLayoutParams(lp);
            WebViewActivity.mTextViewPercentage.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            WebViewActivity.mTextViewPercentageValue.setText(percent+"%");
        }catch (Exception e){
            Log.e("TAG", "Exception: " + e.toString());
        }

        super.onScrollChanged(l, t, oldl, oldt);
        if(mOnScrollChangedCallback != null) mOnScrollChangedCallback.onScroll(l, t);
    }

    public OnScrollChangedCallback getOnScrollChangedCallback()
    {
        return mOnScrollChangedCallback;
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback)
    {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Impliment in the activity/fragment/view that you want to listen to the webview
     */
    public static interface OnScrollChangedCallback
    {
        public void onScroll(int l, int t);
    }
}