package org.bethalevi.sidur1.objects;

/**
 * Created by Mitesh Patel on 11/4/2015.
 */
public class SidurField {

    private int fieldId,value;
    private String startTag,endTag;

    public String getPattern() {
       return  "(?<=\\b"+this.startTag+"\\b).*?(?=\\b"+this.endTag+"\\b)";
    }

    private String pattern;

    public int getFieldId() {
        return fieldId;
    }

    public void setFieldId(int fieldId) {
        this.fieldId = fieldId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getStartTag() {
        return startTag;
    }

    public void setStartTag(String startTag) {
        this.startTag = startTag;
    }

    public String getEndTag() {
        return endTag;
    }

    public void setEndTag(String endTag) {
        this.endTag = endTag;
    }

    public SidurField() {
    }

}
