package org.bethalevi.sidur1.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.bethalevi.sidur1.objects.SidurDate;
import org.bethalevi.sidur1.objects.SidurField;

import java.util.ArrayList;

public class DatabaseFunctions {

	private static final String tag = DatabaseFunctions.class.getSimpleName();
	static SQLiteDatabase db;
	static DataBaseHelper dbHelper;

	public static SidurDate getSidurDate(String date) {
		Cursor cursor = null;
		Log.e("DatabaseFunctions", "date " + date);
		SidurDate sidurDate = new SidurDate();
		try {
			cursor = db
					.rawQuery("SELECT * FROM "+DataBaseHelper.DB_TABLE_NAME + " WHERE ID = '" + date+"'", null);

			if (cursor != null) {

				while (cursor.moveToNext()) {
					sidurDate.setDate(""+cursor.getString(0));
					sidurDate.setDesc("" + cursor.getString(1));
					ArrayList<SidurField> arraySidurField = new ArrayList<SidurField>();
					for (int i = 2; i<99 ; i++){
						int fieldValue = i-1;
						SidurField sidurField = new SidurField();
						sidurField.setFieldId(fieldValue);
						sidurField.setValue(cursor.getInt(i));
						if (fieldValue < 10){
							sidurField.setStartTag("XXX00" + fieldValue);
							sidurField.setEndTag("00"+fieldValue+"XXX");
						}else if (fieldValue < 100){
							sidurField.setStartTag("XXX0" + fieldValue);
							sidurField.setEndTag("0"+fieldValue + "XXX");
						}else if (fieldValue > 99){
							sidurField.setStartTag("XXX" + fieldValue);
							sidurField.setEndTag(fieldValue + "XXX");
						}
						arraySidurField.add(sidurField);
					}

					sidurDate.setArrayListSidurField(arraySidurField);

				}
				return sidurDate;
			} else {
				Log.e(tag, "getSidurDate curcle null " );
				return sidurDate;
			}

		} catch (Exception e) {
			Log.e(tag, "getSidurDate Exception : " + e.toString());
			return sidurDate;
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public static void openDB(Context context) {
		dbHelper = new DataBaseHelper(context);
		
		try {
			dbHelper.createDataBase();
			dbHelper.openDataBase();
			
			if (db != null) {
				if (!db.isOpen()) {
					db = dbHelper.getReadableDatabase();
				}
			} else {
				db = dbHelper.getReadableDatabase();
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("OpenDatabase", "Error : " + e.toString());
		}
		
	}

	public static void closeDB() {
		if (db.isOpen()) {
			db.close();
			dbHelper.close();
		}
	}
}
