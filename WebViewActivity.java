package org.bethalevi.sidur1;
//change by nimesh
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.bethalevi.sidur1.analytics.AnalyticsApplication;
import org.bethalevi.sidur1.database.DatabaseFunctions;
import org.bethalevi.sidur1.objects.Globle;
import org.bethalevi.sidur1.objects.ObservableWebView;
import org.bethalevi.sidur1.objects.SidurDate;
import org.bethalevi.sidur1.objects.SidurField;

import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//changes made by mitesh
public class WebViewActivity extends AppCompatActivity {

    private ObservableWebView mWebViewHTML;
    int zoomValue = 90;
    private ActionBar mActionBar;
    private String mTitle="",url="",mMenuItemMarker="",mScreenName="",mHTMLFileName="";
    private int mPosition;
    public static RelativeLayout mLayoutPregress;
    public static TextView mTextViewPercentage,mTextViewPercentageValue;
    public static float viewWidth;
    private SidurDate mSidurDate;
    private ProgressBar mProgressBar;
    private int mLastT = 1;
    private LinearLayout mLayoutMain;
    private boolean isMarkerLoad = false,isScroll = false,isAllowPercentage=false,isZoom=false;

    public static int mPercentage = 0, mTotalHieght = 0;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        getBundleValues();
        setActionBarValue();

        try {
            AnalyticsApplication application = (AnalyticsApplication) getApplication();
            mTracker = application.getDefaultTracker();

        }catch (Exception e){
            Log.e("AnalyticsApplication", "Exception : " + e.toString());
        }

        initGloble(); //comment by nimesh

    }

    public void getBundleValues() {
        try { //some inbetween comment
            Intent intent = getIntent();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                mTitle = bundle.getString("MenuItemTitle");
                mHTMLFileName = bundle.getString("MenuItemHTMLFileName");
                mMenuItemMarker = bundle.getString("MenuItemMarker");
                mScreenName = bundle.getString("MenuItemScreenName");
                isAllowPercentage = bundle.getBoolean("MenuItemPercentage");
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e("mMenuItem","Exception: "+e.toString());

        }
    }
    public void setActionBarValue(){
        try {
            mActionBar = getSupportActionBar();
            mActionBar.setDisplayUseLogoEnabled(true);
            mActionBar.setDisplayShowHomeEnabled(true);
            mActionBar.setShowHideAnimationEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);
          //  mActionBar.setLogo(getResources().getDrawable(R.drawable.ic_sidur));
            mActionBar.setTitle(" " + mTitle);
        }catch (Exception e){
            Log.e("ActionBar", "Exception : " + e.toString());
        }
    }
    public void initGloble(){ //this is by mitesh

        try {
            mLayoutMain = (LinearLayout)findViewById(R.id.layout_inner_actionbar);
            mProgressBar = (ProgressBar)findViewById(R.id.progressbar);
            mWebViewHTML = (ObservableWebView)findViewById(R.id.webview_html);
            mLayoutPregress = (RelativeLayout)findViewById(R.id.layout_progressbar);
            mTextViewPercentage = (TextView)findViewById(R.id.textview_percentage);
            mTextViewPercentageValue = (TextView)findViewById(R.id.textview_percentage_value);
        }catch (Exception e){
            Log.e("initGloble", "Exception : " + e.toString());
        }
        try {
            viewWidth = mLayoutPregress.getLayoutParams().width;
            ViewTreeObserver vto = mLayoutPregress.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    viewWidth = mLayoutPregress.getMeasuredWidth();//mitesh patels  & nimesh pat4ls changes.


                }
            });
            new ReadHTMLFile().execute();
        }catch (Exception e){
            Log.e("ViewTreeObserver", "Exception : " + e.toString());
        }
    }

    class ReadHTMLFile extends AsyncTask<String,String,String> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            try {
                mProgressBar.setVisibility(View.VISIBLE);

            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        @Override
        protected String doInBackground(String... arg0) {
            // TODO Auto-generated method stub
            String msg = "Error";
            try {
                readHTMLFileTag();
            } catch (Exception e) {
                Log.e("ReadHTMLFile", "Exception: " + e.toString());
            }
            return msg;
        }

        @Override
        protected void onPostExecute(String msg) {
            try {
                loadHtml();
                mProgressBar.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }
    public void readHTMLFileTag(){
        String s ="",newURL="";
        String hideList = "";
        String sArray[] ;

/*        try {
            InputStream htmlStream;
            Reader is = null;
            htmlStream = getAssets().open(mHTMLFileName);
            is = new BufferedReader(new InputStreamReader(htmlStream, "UTF8"));
            final char[] buffer = new char[1024];
            StringBuilder out = new StringBuilder();
            int read;
            do {
                read = is.read(buffer, 0, buffer.length);
                if (read>0) {
                    out.append(buffer, 0, read);
                }
            } while (read>=0);

            s =  out.toString();
            Log.e("s","s : "+s );
            hideList = s.substring(s.indexOf("<!--("), s.indexOf(")-->\n"));
            Log.e("hideList", "hideList: " + hideList);
        }catch (Exception e){
            Log.e("readHTMLFileTag","InputStream Exception: "+e.toString());
        }*/

       try{
           InputStream htmlFile =getAssets().open(mHTMLFileName);
           int size = htmlFile.available();
           byte[] buffer = new byte[size];
           htmlFile.read(buffer);
           htmlFile.close();
           s = new String(buffer);
           s= s.replace("<body>", "<body><br><br><br>");

       }catch (Exception e){
           Log.e("readHTMLFileTag","InputStream Exception: "+e.toString());
       }

        try {
            StringBuilder sb = new StringBuilder();
            sb.append(s);
            hideList = sb.substring(sb.indexOf("<!--(")+5,sb.indexOf(")-->"));
            Log.e("hideList", "hideList: " + hideList);
           /* sArray = hideList.split(",");
           for (int i = 0 ; i < sArray.length ; i++ ){
               sArray[i] = "XXX"+sArray[i];
                Log.e("hideList", "sArray: " + sArray[i]);
            }*/

        }catch (Exception e){
            Log.e("readHTMLFileTag","hideList Exception: "+e.toString());
        }
        try{
            newURL = s;
            mSidurDate = DatabaseFunctions.getSidurDate(""+ Globle.getCurrentDate());
            int arraysize = mSidurDate.getArrayListSidurField().size();
            Log.e("readHTMLFileTag", "mSidurDate: " + mSidurDate.getDate());
            Log.e("readHTMLFileTag", "getArrayListSidurField: " + mSidurDate.getArrayListSidurField().size());
          //  for (int i = arraysize - 1; i >= 0; i--) {

            SidurField sidur;
            for (int i = 0 ; i < arraysize; i ++){
                sidur = mSidurDate.getArrayListSidurField().get(i);
                if (sidur.getValue() == 0){
                  //  if (hideList.contains(mSidurDate.getArrayListSidurField().get(i).getStartTag())){
                    Log.e("readHTMLFileTag", "Loop");

                          Pattern p = Pattern.compile(sidur.getPattern(), Pattern.DOTALL);
                        //Pattern p = Pattern.compile("(?<=\\bXXX007\\b).*?(?=\\b007XXX\\b)", Pattern.DOTALL);
                    Log.e("readHTMLFileTag", "Loop1");

                        Matcher m = p.matcher(newURL.toString());
                    Log.e("readHTMLFileTag", "Loop2");
                      //  Log.e("outer", "m.group(): " +m.group());
                        while (m.find()) {
                            Log.e("inner", "m.group(): " +m.group());
                            newURL=newURL.replace(m.group(),"");
                            Log.e("readHTMLFileTag", "Loop3");
                            // newURL=newURL.replace(m.,"");
                        }
                 //   }
                }
               // newURL=newURL.replace(mSidurDate.getArrayListSidurField().get(i).getStartTag(),"");
              //  newURL=newURL.replace(mSidurDate.getArrayListSidurField().get(i).getEndTag(),"");
            }
        }catch (Exception e){
            Log.e("readHTMLFileTag","Pattern Exception: "+e.toString());
        }
        url = newURL;

    }
    public void loadHtml() {

        mWebViewHTML.getSettings().setJavaScriptEnabled(true);
        mWebViewHTML.setHorizontalScrollBarEnabled(false);
        mWebViewHTML.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        mWebViewHTML.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
        mWebViewHTML.setLongClickable(false);
        mWebViewHTML.setHapticFeedbackEnabled(false);
        WebViewActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mWebViewHTML.loadDataWithBaseURL("file:///android_asset/", url, "text/html", "utf-8", null);
                return;
            }
        });
        mWebViewHTML.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(final WebView view, final String url) {

                super.onPageFinished(view, url);
                Handler lHandler = new Handler();
                lHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!mHTMLFileName.equals("")) {
                            if (mHTMLFileName.equals("03_SHACHRIT.html")) {
                                if (!isMarkerLoad) {
                                    String id = "#" + mMenuItemMarker;
                                    view.loadUrl(url + id);
                                    isMarkerLoad = true;
                                    isScroll = true;
                                }

                            } else {
                                view.scrollTo(0, 1);
                                mWebViewHTML.scrollTo(0, 1);
                            }
                        }
                    }
                }, 600);
            }


        });
        mWebViewHTML.setOnScrollChangedCallback(new ObservableWebView.OnScrollChangedCallback() {
            @Override
            public void onScroll(int l, int t) {

                if (isAllowPercentage) {
                    if (mPercentage == 20 || mPercentage == 40 || mPercentage == 60 || mPercentage == 80 || mPercentage == 100) {
                        mTracker.setScreenName("" + mScreenName + " with " + mPercentage + "%");
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("Action")
                                .setAction("Click")
                                .build());
                    }
                }
         /*       if (!isZoom) {
                    if (!isScroll) {
                        if (mLastT >= t) {
                            if (!mActionBar.isShowing()) {
                                mActionBar.show();
                            }

                        } else {

                            if (mActionBar.isShowing()) {
                                mActionBar.hide();
                            }
                        }
                        mLastT = t;
                    } else {
                        isScroll = false;
                    }
                }*/

                    if (!isScroll) {
                        if (mLastT >= t) {
                            if (!isZoom) {
                                if (!mActionBar.isShowing()) {
                                    mActionBar.show();
                                }
                            }

                        } else {

                            if (!isZoom) {
                                if (mActionBar.isShowing()) {
                                    mActionBar.hide();
                                }
                            }
                        }
                        mLastT = t;
                    } else {
                        isScroll = false;
                    }



            }
        });
        mWebViewHTML.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                isZoom = false;
                return false;
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.zoom, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_zoom_in:
                zoomValue = zoomValue + 20;
                if (zoomValue > 250)
                    zoomValue = 250;
              try {
                  isZoom = true;
                 // mWebViewHTML.zoomIn();
                  mWebViewHTML.getSettings().setTextZoom(zoomValue);
                  Log.e("TAG", "zoomValue: " + zoomValue);
                  Log.e("TAG", "mLastT: " + mLastT);

                   /* int newScroll = ( mPercentage * mTotalHieght ) / 100;
                  Log.e("TAG", "mPercentage: " + mPercentage);
                  Log.e("TAG", "mTotalHieght: " + mTotalHieght);
                  Log.e("TAG", "newScroll: " + newScroll);
                  mWebViewHTML.scrollTo(0, newScroll);*/
              } catch (Exception e) {
                  mWebViewHTML.getSettings().setTextZoom(zoomValue);
              }
                //mWebViewHTML.scrollTo(0, mLastT + 1);
                return true;
            case R.id.action_zoom_out:
                zoomValue = zoomValue - 20;
                if (zoomValue < 80)
                    zoomValue = 70;
               try {
                   isZoom = true;
                  // mWebViewHTML.zoomOut();
                   mWebViewHTML.getSettings().setTextZoom(zoomValue);
               }catch (Exception e){
                   mWebViewHTML.getSettings().setTextZoom(zoomValue);
               }
                return true;
            case 16908332:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


